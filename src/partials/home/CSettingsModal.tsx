import {
  Button,
  Checkbox,
  Col,
  Divider,
  Input,
  Modal,
  Row,
  Typography
} from "antd";
import { inject, observer } from "mobx-react";
import React, { Component } from "react";

import { IStores } from "../../CApp";

interface CSettingsModalProps {
  stores?: IStores;
}

@inject("stores")
@observer
export default class CSettingsModal extends Component<CSettingsModalProps> {
  private stores = this.props.stores!;

  render() {
    return (
      <Modal
        okButtonProps={{ disabled: !this.stores.album.canEdit }}
        title="Album settings"
        visible={this.stores.file.modals[0]}
        width={250}
        onCancel={() => this.stores.file.toggleModal(0)}
        onOk={() => this.stores.file.toggleModal(0)}
      >
        <Row justify="center" type="flex">
          <Col>
            <Button
              disabled={this.stores.album.canEdit}
              type="primary"
              onClick={() => this.stores.album.createAlbum()}
            >
              Create a new album
            </Button>
          </Col>
        </Row>

        <Row>
          <Col>
            <Divider />
          </Col>
        </Row>

        <Row>
          <Col>
            <Typography.Text strong>Or enter your credentials</Typography.Text>
          </Col>
        </Row>

        <Row>
          <Col>
            <Input
              allowClear
              placeholder="input album id"
              value={this.stores.album.albumId}
              onChange={e => (this.stores.album.albumId = e.target.value)}
            />
          </Col>
        </Row>

        <Row style={{ padding: "5px 0px 0px 0px" }}>
          <Col>
            <Input
              allowClear
              placeholder="input album secret"
              value={this.stores.album.secret}
              onChange={e => (this.stores.album.secret = e.target.value)}
            />
          </Col>
        </Row>

        <Row>
          <Col>
            <Divider />
          </Col>
        </Row>

        <Row>
          <Col>
            <Checkbox
              checked={this.stores.file.addFilesToAlbum}
              onChange={e =>
                (this.stores.file.addFilesToAlbum = e.target.checked)
              }
            >
              Add all images
            </Checkbox>
          </Col>
        </Row>
      </Modal>
    );
  }
}
