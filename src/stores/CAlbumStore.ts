import { message } from "antd";
import axios, { AxiosResponse } from "axios";
import { action, computed, observable, reaction } from "mobx";

import extractMatches from "../utils/extractMatches";
import getImageSize from "../utils/getImageSize";

export interface AlbumEntry {
  createdAt: number;
  description: string;
  dimensions?: { height: number; width: number };
  fileId: string;
  title: string;
  updatedAt: number;
}

interface Album {
  createdAt: number;
  description: string;
  entries: AlbumEntry[];
  secret?: string;
  title: string;
  updatedAt: number;
}

// TODO: album id removed = state reset + preserve id on page changes
export class CAlbumStore {
  @observable
  _canEdit: boolean = false;

  @observable
  album: Album = {
    createdAt: -1,
    description: "",
    entries: [],
    title: "",
    updatedAt: -1
  };

  @observable
  albumId: string = "";

  @observable
  dirty: boolean = false;

  @observable
  editing: string = "";

  @observable
  inputFile: string = "";

  @observable
  isCreated: boolean = false;

  @observable
  isProcessing: boolean = false;

  @observable
  modals: [boolean, boolean] = [false, false];

  @observable
  secret: string = "";

  @observable
  selectedEntries: number[] = [];

  @observable
  view: "grid" | "light" = "light";

  constructor() {
    // Load new album if id changes
    reaction(
      () => this.albumId,
      data => {
        if (data.length !== 8) {
          return;
        }

        this.loadAlbum(data);
        this.updateCanEdit();
      }
    );

    // Change _canEdit if secret changes
    reaction(() => this.secret, () => this.updateCanEdit());
  }

  @computed
  get canEdit(): boolean {
    return this._canEdit;
  }

  @computed
  get loaded(): boolean {
    return this.album.createdAt > -1;
  }

  @action
  async addEntriesByList(list: string, onFinish?: Function): Promise<void> {
    const matches = await extractMatches(list);

    if (matches.length < 1) {
      return;
    }

    for (let index = 0; index < matches.length; index++) {
      const { fileId, metadata } = matches[index];

      if (metadata.mimeType.indexOf("image") === -1) {
        return;
      }

      try {
        this.addRawEntry(
          await getImageSize(`https://s.put.re/${fileId}`),
          fileId,
          metadata.originalName
        );

        onFinish && onFinish();
      } catch (_) {}
    }
  }

  @action
  async addEntryByFile(file: MyUploadFile, onFinish?: Function): Promise<void> {
    if (
      !file.response ||
      !file.response.data ||
      file.response.status !== "success"
    ) {
      return;
    }

    if (file.type.indexOf("image") === -1) {
      return;
    }

    try {
      this.addRawEntry(
        await getImageSize(URL.createObjectURL(file.originFileObj)),
        file.response.data.name,
        file.response.data.originalName
      );

      onFinish && onFinish();
    } catch (_) {}
  }

  private addRawEntry(
    dimensions: { height: number; width: number },
    fileId: string,
    title: string
  ) {
    this.album.entries.push({
      createdAt: Date.now(),
      description: "",
      dimensions,
      fileId,
      title,
      updatedAt: Date.now()
    });

    this.dirty = true;
  }

  @action
  async createAlbum() {
    try {
      const response: AxiosResponse<APIAlbumCreateResponse> = await axios.post(
        "https://api.put.re/album/create"
      );

      if (response.data.status === "success" && response.data.data) {
        this.albumId = response.data.data.id;
        this.isCreated = true;
        this.secret = response.data.data.secret;
      }
    } catch (_) {
      // TODO: handle this
    }
  }

  @action
  deleteEntry(index: number): void {
    this.album.entries.splice(index, 1);
    this.dirty = true;
  }

  // TODO Use a method that allows swapping checked items
  @action
  deleteSelectedEntries() {
    this.album.entries = this.album.entries.filter((_, index) => {
      return this.selectedEntries.indexOf(index) === -1;
    });

    this.dirty = true;
    this.selectedEntries = [];
  }

  private async loadAlbum(albumId: string): Promise<void> {
    try {
      const { data } = await axios.get(
        `https://api.put.re/album/${albumId}.json`
      );

      if (data.status === "success" && data.data) {
        this.album = data.data;
      }
    } catch (_) {
      // TODO handle this
    }
  }

  @action
  setAlbumTitle(text: string): void {
    if (text.length > 50) {
      message.error("Title has more than 50 characters");
      return;
    }

    this.album.title = text;
    this.dirty = true;
    this.editing = "";
  }

  @action
  setFileDescription(index: number, text: string): void {
    if (text.length > 100) {
      message.error("Description has more than 100 characters");
      return;
    }

    this.album.entries[index].description = text;
    this.album.entries[index].updatedAt = Date.now();
    this.dirty = true;
    this.editing = "";
  }

  @action
  setFileTitle(index: number, text: string): void {
    if (text.length > 50) {
      message.error("Title has more than 50 characters");
      return;
    }

    this.album.entries[index].title = text;
    this.album.entries[index].updatedAt = Date.now();
    this.dirty = true;
    this.editing = "";
  }

  @action
  swapEntries(x: number, y: number): void {
    [this.album.entries[x], this.album.entries[y]] = [
      this.album.entries[y],
      this.album.entries[x]
    ];

    this.album.entries[x].updatedAt = Date.now();
    this.album.entries[y].updatedAt = Date.now();
    this.dirty = true;
  }

  @action
  async sync(): Promise<void> {
    try {
      await axios.post(`https://api.put.re/album/${this.albumId}/modify`, {
        ...this.album,
        ...{ secret: this.secret, updatedAt: Date.now() }
      });

      this.dirty = false;
    } catch (_) {
      message.error("Request failed! Please try again later.");
    }
  }

  @action
  toggleModal(index: number): void {
    this.modals[index] = !this.modals[index];
  }

  @action
  toggleSelectedEntry(index: number): void {
    const idx = this.selectedEntries.indexOf(index);

    if (idx === -1) {
      this.selectedEntries.push(index);
      return;
    }

    this.selectedEntries.splice(idx, 1);
  }

  @action
  toggleView(): void {
    this.view = this.view === "grid" ? "light" : "grid";
  }

  private async updateCanEdit(): Promise<void> {
    if (this.secret.length !== 8 || this.albumId.length !== 8) {
      this._canEdit = false;
      return;
    }

    try {
      await axios.post(`https://api.put.re/album/${this.albumId}/modify`, {
        secret: this.secret
      });
    } catch (error) {
      if (
        error.response &&
        error.response.data.message.indexOf("object") !== -1
      ) {
        this._canEdit = true;
      }
    }
  }
}

export default new CAlbumStore();
