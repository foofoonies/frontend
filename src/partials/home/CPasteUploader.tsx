import { message } from "antd";
import axios, { AxiosResponse } from "axios";
import { inject, observer } from "mobx-react";
import React, { Component } from "react";

import { IStores } from "../../CApp";
import spreadFile from "../../utils/spreadFile";

interface CPasteUploaderProps {
  stores?: IStores;
}

@inject("stores")
@observer
export default class CPasteUploader extends Component<CPasteUploaderProps> {
  private store = this.props.stores!.file;

  componentDidMount() {
    window.addEventListener("paste", event => {
      if (this.store.uploader === "text") {
        return;
      }

      if (this.store.modals.some(value => value)) {
        return;
      }

      const items = (
        (event as any).clipboardData ||
        (event as any).originalEvent.clipboardData
      ).items;

      for (let index = 0; index < items.length; index += 1) {
        const item = items[index];

        if (item.kind === "file") {
          const blob = item.getAsFile();
          const file = new File(
            [blob],
            `pasted-blob.${blob.type.match(/(?:[^/]*\/)([^;]*)/)[1]}`
          );

          this.upload(file);
        } else if (item.kind === "string") {
          // This prevents some weird bug, where you have 2 files in the queue and the first one is screwd up html
          if (items.length > 1 && index === 0) {
            continue;
          }

          item.getAsString((str: string) => {
            const blob = new Blob([str], { type: "text/plain" });
            const file = new File([blob], "pasted-text.txt");

            this.upload(file);
          });
        }
      }
    });
  }

  private async upload(file: File): Promise<void> {
    const formData = new FormData();
    formData.append("file", file);

    try {
      const response: AxiosResponse<APIUploadResponse> = await axios.post(
        "https://api.put.re/upload",
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data"
          }
        }
      );

      this.store.upsertEntry(
        spreadFile(file, {
          percent: 100,
          response: response.data,
          status: "done"
        })
      );
    } catch (_) {
      message.error("Request failed! Please try again later.");
    }
  }

  render() {
    return <></>;
  }
}
