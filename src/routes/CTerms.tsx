import { Col, Divider, Row, Typography } from "antd";
import React, { PureComponent } from "react";

const { Title, Paragraph } = Typography;

export default class CTerms extends PureComponent {
  render() {
    return (
      <>
        <Row>
          <Col>
            <Title>Terms of service</Title>
          </Col>
        </Row>

        <Row>
          <Col>
            <Divider />
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>§1</Title>
            <Paragraph>
              Copyrighted material, unless you have the permission from the
              copyright holder, is not allowed on put.re's servers if you share
              the link in public. In case of a valid copyright claim the said
              material will be deleted from the servers.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>§2</Title>
            <Paragraph>
              Spamming put.re's servers by mass-uploading the same file, or
              spamming of any kind is strictly prohibited. Your access to the
              service will be terminated if we feel you have a malicious intent.
              No exceptions.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>§3</Title>
            <Paragraph>
              We do not allow illegal content of any kind on our servers. This
              includes but is not limited to child pornography and any depiction
              of drugs ranging from consumption to instructional photos or
              videos.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>§4</Title>
            <Paragraph>
              Pornographic content is permitted as long as it's legal.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>§5</Title>
            <Paragraph>
              If you upload pornographic content you agree to not share it with
              minors as it constitutes a crime in most countries.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>§6</Title>
            <Paragraph>
              You agree to the fact that your data might be stored both on our
              European and United States servers.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>§7</Title>
            <Paragraph>
              You agree to the fact that we cannot be held responsible in any
              way if your uploaded material gets copied or used by a third party
              using our service.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>§8</Title>
            <Paragraph>
              It's likely that we might change our Terms of Service in the
              future without any notifications. To assure you're aware of our
              rules you shall review our Terms of Service as often as you can.
            </Paragraph>
          </Col>
        </Row>
      </>
    );
  }
}
