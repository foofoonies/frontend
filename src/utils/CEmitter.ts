export default class CEmitter {
  private _callbacks: any = {};

  on(event: string, fn: Function) {
    this._callbacks = this._callbacks || {};

    if (!this._callbacks[event]) {
      this._callbacks[event] = [];
    }

    this._callbacks[event].push(fn);

    return this;
  }

  emit(event: string, ...args: any[]) {
    this._callbacks = this._callbacks || {};
    let callbacks = this._callbacks[event];

    if (callbacks) {
      for (let callback of callbacks) {
        callback.apply(this, args);
      }
    }

    return this;
  }

  off(event: string, fn: Function) {
    if (!this._callbacks || arguments.length === 0) {
      this._callbacks = {};
      return this;
    }

    let callbacks = this._callbacks[event];
    if (!callbacks) {
      return this;
    }

    if (arguments.length === 1) {
      delete this._callbacks[event];
      return this;
    }

    for (let index = 0; index < callbacks.length; index++) {
      let callback = callbacks[index];
      if (callback === fn) {
        callbacks.splice(index, 1);
        break;
      }
    }

    return this;
  }
}
