import { action, autorun, computed, observable, reaction } from "mobx";

import CAlbumStoreInstance from "./CAlbumStore";

export class CFileStore {
  @observable
  _addedFiles: string[] = [];

  @observable
  addFilesToAlbum: boolean = true;

  @observable
  files: MyUploadFile[] = [];

  @observable
  modals: [boolean] = [false];

  @observable
  uploader: "file" | "text" = "file";

  constructor() {
    autorun(() => {
      const average =
        this.files.reduce((total, value) => total + value.percent!, 0) /
        this.files.length;

      if (average < 100) {
        document.title = `put.re - ${Math.round(average).toFixed(2)} %`;
        return;
      }

      document.title = "put.re";
    });

    reaction(
      () =>
        this.files
          .filter(
            value =>
              this._addedFiles.indexOf(value.uid) === -1 &&
              value.status === "done" &&
              value.type.indexOf("image") > -1
          )
          .map(value => value),
      data =>
        this.addFilesToAlbum &&
        CAlbumStoreInstance.loaded &&
        data.forEach(value => {
          this._addedFiles.push(value.uid);
          CAlbumStoreInstance.addEntryByFile(value);
        })
    );
  }

  @computed
  get hasActiveUploads(): boolean {
    let flag = false;

    this.files.forEach(value => {
      if (value.status !== "done") {
        flag = true;
      }
    });

    return flag;
  }

  @action
  clear(): void {
    this.files = [];
  }

  @action
  toggleModal(index: number): void {
    this.modals[index] = !this.modals[index];
  }

  @action
  toggleUploader(): void {
    this.uploader = this.uploader === "file" ? "text" : "file";
  }

  @action
  upsertEntry(file: MyUploadFile): void {
    const index = this.files.findIndex(value => value.uid === file.uid);

    if (index === -1) {
      this.files.push(file);
      return;
    }

    this.files[index] = file;
  }
}

export default new CFileStore();
