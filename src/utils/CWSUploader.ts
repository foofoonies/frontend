import { RcFile } from "antd/lib/upload/interface";

import CEmitter from "./CEmitter";

const chunkSize = 3 * 1024 * 1024;
const maxBuffer = 50 * 1024 * 1024;
const progressInterval = 250;

export default class CWSUploader extends CEmitter {
  private url: string = "";
  private queue: RcFile[] = [];
  private currentFile?: RcFile;
  private ws: WebSocket | undefined;
  private pulseTimer: any = 0;
  private connecting: boolean = false;
  private connected: boolean = false;
  private startUploadSent: boolean = false;
  private position: number = 0;
  private startTime: Date = new Date();
  private lastStatsPrintTime: Date = new Date();

  constructor(url: string) {
    super();

    this.url = url;
  }

  setupPulse() {
    if (this.pulseTimer !== 0) {
      return;
    }

    this.pulseTimer = setTimeout(() => {
      this.pulse();
    }, 1);
  }

  connect() {
    if (!this.connected && !this.connecting) {
      this.connecting = true;
      this.ws = new WebSocket(this.url);
      this.ws.binaryType = "blob";

      this.ws.onopen = () => {
        this.connecting = false;
        this.connected = true;
      };

      this.ws.onmessage = (ev: MessageEvent) => {
        if (ev.data === "error") {
          this.onFinish(new Error("error uploading"), undefined);
        } else {
          try {
            const data = JSON.parse(ev.data);
            this.onFinish(null, data);
          } catch (_) {}
        }
      };

      this.ws.onclose = () => {
        if (this.currentFile !== undefined) {
          this.onFinish(new Error("Socket closed"), undefined);
        }

        this.connected = false;
        this.connecting = false;
        this.ws = undefined;
      };
    }
  }

  disconnect(): void {
    if (this.currentFile === undefined && this.queue.length === 0) {
      try {
        this.ws && this.ws.close();
      } catch (_) {}
    }
  }

  onFinish(error: Error | null, response: APIUploadResponse | undefined): void {
    setTimeout(() => {
      if (this.queue.length === 0 && this.currentFile === undefined) {
        clearTimeout(this.pulseTimer);
        this.pulseTimer = 0;

        if (this.ws) {
          this.ws.close();
        }
      }
    }, 5000);

    if (error) {
      this.emit("error", this.currentFile, error);
    } else {
      const speed = this.currentFile
        ? this.currentFile.size /
          1024 /
          1024 /
          Math.round((Date.now() - this.startTime.getTime()) / 1000)
        : 0;

      this.emit("finish", this.currentFile, response, speed);
    }

    this.currentFile = undefined;
  }

  pulse(): void {
    if (this.currentFile === undefined && this.queue.length > 0) {
      this.connect();

      this.currentFile = this.queue.shift();
      this.startUploadSent = false;
      this.position = 0;
      this.startTime = new Date();

      this.emit("start", this.currentFile);
    }

    if (this.currentFile !== undefined && this.connected && this.ws) {
      if (!this.startUploadSent) {
        this.startUploadSent = true;

        this.ws.send(
          JSON.stringify({
            type: "startUpload",
            data: {
              filename: this.currentFile.name,
              size: this.currentFile.size
            }
          })
        );
      } else if (
        this.position < this.currentFile.size &&
        this.ws.bufferedAmount < maxBuffer
      ) {
        const size = Math.min(chunkSize, this.currentFile.size - this.position);
        const start = this.position;
        const end = start + size;
        this.position += size;

        this.ws.send(this.currentFile.slice(start, end));
      }

      if (this.lastStatsPrintTime.getTime() + progressInterval < Date.now()) {
        this.lastStatsPrintTime = new Date();

        const sent = this.position - this.ws.bufferedAmount;
        const elapsed = Math.round(
          (Date.now() - this.startTime.getTime()) / 1000
        );
        const speed = sent / 1024 / 1024 / elapsed;
        const progress = Math.max(0, (sent / this.currentFile.size) * 100);

        this.emit("progress", this.currentFile, progress, speed);
      }
    }

    if (this.pulseTimer !== 0) {
      this.pulseTimer = setTimeout(() => {
        this.pulse();
      }, 1);
    }
  }

  queueFile(file: RcFile): void {
    this.setupPulse();
    this.queue.push(file);
  }
}
