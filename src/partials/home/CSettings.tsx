import { Icon } from "antd";
import { inject, observer } from "mobx-react";
import React, { Component, lazy, Suspense } from "react";

import { IStores } from "../../CApp";

const CSettingsModal = lazy(() =>
  import(/* webpackPrefetch: true */ "./CSettingsModal")
);

interface CSettingsProps {
  stores?: IStores;
}

@inject("stores")
@observer
export default class CSettings extends Component<CSettingsProps> {
  private stores = this.props.stores!;

  render() {
    return (
      <>
        <Suspense fallback={null}>
          {this.stores.file.modals[0] ? <CSettingsModal /> : null}
        </Suspense>

        <Icon
          theme="filled"
          title="Settings"
          type="setting"
          onClick={() => this.stores.file.toggleModal(0)}
        />
      </>
    );
  }
}
